#!env python2
"""traverso.py - a proof of concept for executing REST service business logic based on a DOT graph"""


import networkx as nx
import tempita
import colorama
from blessings import Terminal
from itertools import *
from more_itertools import *


term = Terminal()

g = nx.read_dot('centralizado.dot')

# safe context, with no access to external code
client_cache = { '__builtins__' : {'len' : len} }

def epsilon_visitor(node):

    assert len(g.edges(node)) == 1, 'epsilon nodes must have only one action'

    start, end, attr = first(g.edges(node, data=True))
    action = attr.get('action')

    assert action == u'\N{Greek Small Letter Epsilon}',  "Invalid action {0}, expecting epsilon".format(action)
    
    return end

def condition_visitor(node, result):

    result_str = 'true' if result else 'false'
    return select_visitor(node, result_str)


def action_visitor(node):

    assert len(g.edges(node)) == 1, 'action nodes must have only one action'

    start, end, attr = first(g.edges(node, data=True))

    action = attr.get('action')

    if action == 'request':
        method = attr.get('method')
        print "\t", term.bold_green(method.upper()), term.green(tempita.sub(attr.get('label'), **client_cache))
        return end
    else:
        return epsilon_visitor(node)

def parallel_visitor(node):

    print "Starting parallel processing"

    join = g.node[node].get('join')

    for thread, (split, next, attr) in enumerate(g.edges(node, data=True)):

        action = attr.get('action')

        assert action == u'\N{Greek Small Letter Epsilon}',  "Invalid action {0}, expecting epsilon".format(action)

        print term.bold_yellow("Thread #{0}".format(thread))

        result = execute_branch(next, join)

        assert result == join

        print term.yellow("Thread #{0} joined on {1}".format(thread, result))



    print term.yellow("All threads from {0} joined on {1}".format(node, join))

    return epsilon_visitor(join)


def select_visitor(node, result):
    next_node = first(end for start, end, data in g.edges_iter(node, True) if data.get('role') == result);

    assert next_node, "Invalid {1} action for node {0}".format(node, result)

    print "\t", "conditional", term.bold(result)
    return next_node

def process_role(node):    
    role = g.node[node].get('role')
    state = g.node[node].get('label')

    if role in ['init', 'loop', 'client']:
        exec state in client_cache, client_cache
        return epsilon_visitor
    elif role == 'conditional':
        # print 'context {0}'.format(client_cache)
        result = bool(eval(state, client_cache))
        return lambda n: condition_visitor(n, result)        
    elif role == 'select':
        result = eval(state, client_cache)
        return lambda n: select_visitor(n, result)
    elif role == 'split':
        return parallel_visitor
    elif role == 'join':
        return lambda n: n
    
    assert role == None, "Unexpected role {0}".format(role.encode('utf-8'))

        
    return action_visitor

def execute_branch(start, end):
    node = start

    while node != None:

        state = g.node[node].get('label')

        assert state, "Found unlabeled node {node}"

        color = term.bold_blue if g.node[node].get('role') else term.blue

        print "state", color(tempita.sub(state, **client_cache))

        if node == end:
            return node

        visitor = process_role(node)

        node = visitor(node)

if __name__ == "__main__":
    start_node = 'client_start'
    end_node = 'order_completed'
    result = execute_branch(start_node, end_node)

    assert end_node == result

    print term.bold_green("Execution completed")
