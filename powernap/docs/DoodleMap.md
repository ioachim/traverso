% DoodleMap

Buscamos hacer una implementación de DoodleMap. Las iteraciones serían

- Modelo funcional hardcoded
- Modelo funcional con ReLL
- Integrar definición de estados
- Construir máquina de estados

Tendríamos un recurso (DoodleMap) y la colección correspondiente

Consideraciones de la Fig. 4:

- Hasta Wait tenemos un request POST (o PUT)
- Desde Wait tenemos otro request GET, que indica si está cerrado