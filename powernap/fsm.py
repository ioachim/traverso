import networkx as nx


def parse(f):
    g = nx.MultiDiGraph()
    for l in f:        
        d = l.split()
        if len(d) == 0:
            continue

        assert len(d) % 2 == 1, 'Invalid line {0}'.format(l)

        # convert row into transition tuples
        transitions = zip( 
            (int(x) for x in d[0:-2:2]), 
            (int(y) for y in d[2::2]), 
            d[1::2]
            #({ 'label' : i } for i in d[1::2]) 
                    )
        
        #print transitions
        for t in transitions:
            g.add_edge(t[0], t[1], label=t[2])
        
        #g.add_edges_from(transitions)

    return g
