import networkx as nx
import fsm
import matplotlib.pyplot as plot
import matplotlib as mpl
from codecs import open 

#mpl.rcParams['text.usetex'] = True
#mpl.rcParams['text.latex.unicode'] = True
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Calibri', 'DejaVu Sans']

fn = 'weather'

g = fsm.parse(open('{0}.fsm'.format(fn), 'r', 'utf-8'))

#print g.edges(data=True)

path = nx.shortest_path(g.reverse(), 11, 0)

for n in path:
    g.node[n]['path'] = True
    g.node[n]['color'] = 'red'

node_colors = [ 'red' if n in path else 'white' for n in g.nodes()  ]
#print [i for i in reversed(path)]
# pos = { 0 : (0,0), 1 : (1, 1), 2 : (1,) }

#pos = nx.pydot_layout(g, 'twopi', root=0)

pos = nx.layout.spring_layout(g, iterations=1, dim=2)
#pos = dict(  (int(n[0]), [float(c) for c in n[1]['pos'].split(',')]) for n in gx.nodes(data=True)  )

labels = dict( ((u, v), d['label']) for u, v, d in g.edges(data=True))

#nx.draw_networkx_edge_labels(g, pos, edge_labels=labels)

#nx.draw_networkx(g, pos, node_color=node_colors)

#plot.show()

nx.to_agraph(g).draw('{0}.svg'.format(fn), prog='dot')