import json
import requests

def refresh_token():
    creds = json.load(open("creds.json"))
    r = requests.post("https://accounts.google.com/o/oauth2/token", data=dict(creds, grant_type = 'refresh_token'))
    return r.json['access_token']
