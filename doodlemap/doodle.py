import requests
import lxml
from bs4 import BeautifulSoup
from urlparse import urljoin, parse_qs
from oauth_hook import OAuthHook
from pprint import pprint

CONSUMER_KEY = 'ledyutxgsu2tpq3u45wg0u9e99af4mwa'
CONSUMER_SECRET = 'd9p9y093tidepkbsmwowobmzzrm2vviz'
BASE_URI = "http://doodle-test.com/api1/"


def request_token(consumer_key, consumer_secret):

    doodle_oauth_hook = OAuthHook(consumer_key=consumer_key, consumer_secret=consumer_secret, header_auth=True)
    response = requests.get(urljoin(BASE_URI, 'oauth/requesttoken'), hooks={'pre_request': doodle_oauth_hook}, params={'doodle_get' :'name|initiatedPolls|participatedPolls|calendars'})
    
    if response.status_code != 200:
        return response
    token = parse_qs(response.text)
    access_token = token['oauth_token'][0]
    access_secret = token['oauth_token_secret'][0]

    return OAuthHook(access_token, access_secret, consumer_key, consumer_secret, header_auth=True)

class Doodle:
    """docstring for Doodle"""

    def __init__(self, oauth_hook):
        self.hook = oauth_hook
        self.session = requests.session(hooks={'pre_request' : oauth_hook})
        
    def get_user(self):
        response = self.session.get(urljoin(BASE_URI, 'user'))
        return response

    def get_poll(self, id):
        response = self.session.get(urljoin(BASE_URI, 'poll/{0}'.format(id)))
        return response