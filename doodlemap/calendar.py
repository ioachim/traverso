from urlparse import urljoin

BASE_URI = "https://www.googleapis.com/calendar/v3"

class CalendarList:
    url = urljoin(BASE_URI, "users/me/calendarList")

    @classmethod
    def list():
        requests.get(url, params={ '' })
        
class Calendar:
    url = urljoin(BASE_URI, "calendar")

    @classmethod
    def get(calendar_id):
        local_url = urljoin(Calendar.url, calendar_id)

class Event:

    def __init__(calendar_id):
        url = urljoin(BASE_URI, "calendar/{calendar_id}/event".format(calendar_id=calendar_id)) 