import argparse, webbrowser, json
import requests
from requests_oauth2 import OAuth2


parser = argparse.ArgumentParser(description='Download credentials from a Google account')
parser.add_argument('-s', '--scope', action='append')
parser.add_argument('-o', '--output', default='creds.json')
parser.add_argument('client_id', type=str)
parser.add_argument('client_secret', type=str)


args = parser.parse_args()

oauth = OAuth2(args.client_id, args.client_secret, "https://accounts.google.com/o/", "urn:ietf:wg:oauth:2.0:oob", 
    authorization_url="oauth2/auth", token_url="oauth2/token")

webbrowser.open_new(oauth.authorize_url(' '.join(args.scope), response_type='code'))

code = raw_input('Input client code: ')

creds = oauth.get_token(code, grant_type='authorization_code')

json.dump(creds, open(args.output, 'w'))